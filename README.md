## REQUIREMENTS
* JDK 6 and above

## INSTALLATION
1. Clone the repository using the following command:

		git clone https://omgwhut@bitbucket.org/teamftw/psd3_sprintone.git
	
## RUNNING THE PROGRAM

1. Open a terminal on your computer and navigate to the folder previously cloned

2. Compile the program by running the following command:

		javac *.java
		
3. To run the program, run Test.

		java Test