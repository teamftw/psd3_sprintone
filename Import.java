import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Import {
    public Import(){

    }
    
    public void createUploader(String filename) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(filename));
    	 
    	//Set the delimiter used in file
    	scanner.useDelimiter(",");
    	 
    	//Get all tokens and store them in some data structure
    	//I am just printing them
    	while (scanner.hasNext())
    	{
  
                
        	System.out.print(scanner.next() + "   ");
                
    	}
    	 
    	//Do not forget to close the scanner
    	scanner.close();
    }
}
