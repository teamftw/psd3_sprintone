import java.util.ArrayList;

public class Student {
    String courseName;
    String idNumber;
    String courseWorkMarks;
    String examMarks;
    String totalMarks;

    public Student(String a, String b, String c, String d, String e) {
        courseName = a;
        idNumber = b;
        courseWorkMarks = c;
        examMarks = d;
        totalMarks = e;
    }
}
