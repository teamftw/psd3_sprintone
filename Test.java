import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class Test {

    private static ArrayList<Course> courseList;
    private static ArrayList<Course> ALG3;
    private static ArrayList<Course> PSD3;
    private static ArrayList<Course> PL3;
    private static ArrayList<Student> studentList;
    private static ArrayList<Course> ALG3Session1;
    private static ArrayList<Course> PL3Session1;
    private static ArrayList<Course> PSD3Session1;
    private static ArrayList<Course> ALG3Session4Attendance;
    private static ArrayList<Course> PL3Session4Attendance;
    private static ArrayList<Course> PSD3Session4Attendance;

    public static void main(String[] args) throws FileNotFoundException {
        //Initialise student and course objects by calling initialiser;
        Initialiser initialise = new Initialiser();
        courseList = initialise.initialiseCourse();
        ALG3 = initialise.initialiseCourse();
        PSD3 = initialise.initialiseCourse();
        PL3 = initialise.initialiseCourse();
        studentList = initialise.initialiseStudent();
        ALG3Session1 = initialise.initialiseSessions();
        PL3Session1 = initialise.initialiseSessions();
        PSD3Session1 = initialise.initialiseSessions();
        ALG3Session4Attendance = initialise.initialiseSessionAttendance();
        PL3Session4Attendance = initialise.initialiseSessionAttendance();
        PSD3Session4Attendance = initialise.initialiseSessionAttendance();

        //Login and display menu by calling in login class
        System.out.println("Welcome to UOG-CS System! To proceed, please enter your username and password. \n" + "Username:");
        Scanner scanner = new Scanner(System.in);
        String username = scanner.next();

 	int choice = 1;
        
        if (username.equals("admin")) {
            choice = 1;
        } else if (username.equals("tutor")) {
            choice = 2;
        }

        switch (choice) {
            case 1:
                System.out.println("Password: ");
                if (scanner.next().equals("password")) {
                    printAdminUI();
                } else {
                    System.out.println("Wrong password entered!");
                    System.exit(0);
                }
            case 2:
                System.out.println("Password: ");
                if (scanner.next().equals("password")) {
                    printTutorUI();
                } else {
                    System.out.println("Wrong password entered!");
                    System.exit(0);
                }
        }
    }

    public static void printAdminUI() {
        // == ADMIN UI==
        //After login is successful, choose which CSV to export
        System.out.println("Choose an option from the menu below:");
        System.out.println("1. Export grades by course for all students");
        System.out.println("2. Export grades for all course for a single student");
        System.out.println("3. View attendance at particular session");
        Export e = new Export();
        Scanner scanner1 = new Scanner(System.in);
        int choice = scanner1.nextInt();

        switch (choice) {
            case 1:
                e.printCourseCSV("courseTest.csv", courseList);
                System.out.println("Grades by course for all students have been exported successfully");
                break;
            case 2:
                e.printStudentCSV("studentTest.csv", studentList);
                System.out.println("Grades by course for single student have been exported successfully");
                break;
            case 3:
                System.out.println("Please select a session from the menu below:");
                System.out.println("1. ALG3Session 4");
                System.out.println("2. PL3Session 4");
                System.out.println("3. PSD3Session 4");
                Scanner scanner4 = new Scanner(System.in);
                int choice1 = scanner4.nextInt();
                if (choice1 == 1) {
                    for (int l = 0; l < ALG3Session1.size(); l++) {
                        System.out.println(ALG3Session4Attendance.get(l).firstName + " " + ALG3Session4Attendance.get(l).lastName + " " + ALG3Session4Attendance.get(l).isPresent);
                    }
                } else if (choice1 == 2) {
                    for (int l = 0; l < PL3Session1.size(); l++) {
                        System.out.println(PL3Session4Attendance.get(l).firstName + " " + PL3Session4Attendance.get(l).lastName + " " + PL3Session4Attendance.get(l).isPresent);
                    }
                }
                if (choice1 == 3) {
                    for (int l = 0; l < PSD3Session1.size(); l++) {
                        System.out.println(PSD3Session4Attendance.get(l).firstName + " " + PSD3Session4Attendance.get(l).lastName + " " + PSD3Session4Attendance.get(l).isPresent);
                    }
                }
        }
        System.exit(0);
    }

    public static void printTutorUI() throws FileNotFoundException {
        // == TUTOR UI==
        //After login is successful, choose which CSV to export
        System.out.println("Choose an option from the menu below:");
        System.out.println("1. Edit attendance manually.");
        System.out.println("2. Upload attendance by csv file.");
        Export e = new Export();
        Scanner scanner1 = new Scanner(System.in);
        int choice = scanner1.nextInt();

        switch (choice) {
            case 1:
                System.out.println("Session List:");
                for (int i = 0; i < ALG3Session1.size(); i++) {
                    System.out.println((i + 1) + " " + ALG3Session1.get(i).firstName + " " + ALG3Session1.get(i).lastName);
                }
                System.out.println("Enter index number of student(s) to mark absent, press 0 to finish marking and exit.");

                Scanner scanner3 = new Scanner(System.in);

                int choice3 = scanner3.nextInt();
                if (choice3 == 0) {
                    System.out.println("Invalid choice selected. Please input anothernumber.");
                    choice3 = scanner3.nextInt();
                }
                do {
                    ALG3Session1.get(choice3 - 1).isPresent = "Absent";
                    Course temp = new Course();
                    temp = ALG3Session1.get(choice3 - 1);
                    ALG3Session1.remove(choice3 - 1);
                    ALG3Session1.add(temp);
                    int i3 = 1;
                    int i4 = ALG3Session1.size(); //Counter used for detecting no more student left in ALG3Session1 for marking absent.
                    for (int d = 0; d < ALG3Session1.size(); d++) {
                        if (ALG3Session1.get(d).isPresent.equals("Present")) {
                            System.out.println((i3) + " " + ALG3Session1.get(d).firstName + " " + ALG3Session1.get(d).lastName);
                            i3++;
                            i4++;
                        }
                    }
                    if (i4 == ALG3Session1.size()) {
                        System.out.println("All students have been marked absent.");
                        System.exit(0);
                    }
                    System.out.println("Enter index number of student(s) to mark absent:");
                    choice3 = scanner3.nextInt();
                } while (choice3 != 0);
                if (choice3 == 0) {
                    System.out.println("All attendance have been marked.");
                    System.exit(0);
                }
            case 2:
                Import in1 = new Import();
                System.out.println("Please input the filename");
                Scanner scanner4 = new Scanner(System.in);
                String filename = scanner4.next();
                in1.createUploader(filename);
        }
        System.exit(0);
    }

}// end of test.java 
