import java.util.ArrayList;

//Class to hard code all student and courses.
public class Initialiser {

    public Initialiser() {
    }

    public ArrayList<Course> initialiseCourse() {
        Course c1 = new Course("Lavender", "Brown", "9801001", "A", "E");
        Course c2 = new Course("Seamus", "Finnigan", "9801002", "P", "P");
        Course c3 = new Course("Hermione", "Granger", "9801003", "O", "O");
        Course c4 = new Course("Neville", "Longbottom", "9801004", "D", "T");
        ArrayList<Course> courseList = new ArrayList<Course>();
        courseList.add(c1);
        courseList.add(c2);
        courseList.add(c3);
        courseList.add(c4);
        return courseList;
    }

    public ArrayList<Student> initialiseStudent() {
        Student s1 = new Student("ADS2", "CompSci", "15", "30", "45");
        Student s2 = new Student("ADS2", "CompSci", "15", "30", "45");
        Student s3 = new Student("ADS2", "CompSci", "15", "30", "45");
        ArrayList<Student> studentList = new ArrayList<Student>();
        studentList.add(s1);
        studentList.add(s2);
        studentList.add(s3);
        return studentList;
    }
    
    public ArrayList<Course> initialiseSessions(){
        Course c1 = new Course("Lavender", "Brown", "9801001", "A", "E", "Present");
        Course c2 = new Course("Seamus", "Finnigan", "9801002", "P", "P", "Present");
        Course c3 = new Course("Hermione", "Granger", "9801003", "O", "O", "Present");
        Course c4 = new Course("Neville", "Longbottom", "9801004", "D", "T", "Present");
        Course c5 = new Course("Ass", "Longbottom", "9801005", "D", "T", "Present");
        ArrayList<Course> courseList = new ArrayList<Course>();
        courseList.add(c1);
        courseList.add(c2);
        courseList.add(c3);
        courseList.add(c4);
        courseList.add(c5);
        return courseList;
    }
    
    public ArrayList<Course> initialiseSessionAttendance(){
        Course c1 = new Course("Lavender", "Brown", "9801001", "A", "E", "Present");
        Course c2 = new Course("Seamus", "Finnigan", "9801002", "P", "P", "Absent");
        Course c3 = new Course("Hermione", "Granger", "9801003", "O", "O", "Present");
        Course c4 = new Course("Neville", "Longbottom", "9801004", "D", "T", "Absent");
        Course c5 = new Course("Ass", "Longbottom", "9801005", "D", "T", "Present");
        ArrayList<Course> courseList = new ArrayList<Course>();
        courseList.add(c1);
        courseList.add(c2);
        courseList.add(c3);
        courseList.add(c4);
        courseList.add(c5);
        return courseList;
    }
}
