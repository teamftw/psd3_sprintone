import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Export {

    private FileWriter fileWriter;
    private PrintWriter printWriter;
    //private Student student;

    public Export() {
    }

    public void printCourseCSV(String sFileName, ArrayList<Course> list) {
        try {
            FileWriter writer = new FileWriter(sFileName);

            writer.append("First Name");
            writer.append(',');
            writer.append("Last Name");
            writer.append(',');
            writer.append("Id");
            writer.append(',');
            writer.append("Assignment 1");
            writer.append(',');
            writer.append("Assignment 2");
            writer.append('\n');

            for (int i = 0; i < list.size(); i++) {
                writer.append(list.get(i).firstName);
                writer.append(',');
                writer.append(list.get(i).lastName);
                writer.append(',');
                writer.append(list.get(i).studentIdNumber);
                writer.append(',');
                writer.append(list.get(i).As1Grade);
                writer.append(',');
                writer.append(list.get(i).As2Grade);
                writer.append('\n');
            }

            //generate whatever data you want

            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printStudentCSV(String sFileName, ArrayList<Student> list) {
        try {
            FileWriter writer = new FileWriter(sFileName);

            for (int i = 0; i < list.size(); i++) {
                writer.append("Course");
                writer.append(',');
                writer.append("ID number");
                writer.append(',');
                writer.append("Coursework");
                writer.append(',');
                writer.append("Exam");
                writer.append(',');
                writer.append("Total");
                writer.append('\n');


                writer.append(list.get(i).courseName);
                writer.append(',');
                writer.append(list.get(i).idNumber);
                writer.append(',');
                writer.append(list.get(i).courseWorkMarks);
                writer.append(',');
                writer.append(list.get(i).examMarks);
                writer.append(',');
                writer.append(list.get(i).totalMarks);
                writer.append('\n');
            }


            //generate whatever data you want

            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
