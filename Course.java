public class Course {

    String firstName;
    String lastName;
    String studentIdNumber;
    String As1Grade;
    String As2Grade;
    String isPresent;
    
    public Course() {
    
    }

    public Course(String strfirstName, String strlastName, String strstudentIdNumber, String strAs1Grade, String strAs2Grade) {
        firstName = strfirstName;
        lastName = strlastName;
        studentIdNumber = strstudentIdNumber;
        As1Grade = strAs1Grade;
        As2Grade = strAs2Grade;
    }
    
    public Course(String strfirstName, String strlastName, String strstudentIdNumber, String strAs1Grade, String strAs2Grade, String strIsPresent) {
        firstName = strfirstName;
        lastName = strlastName;
        studentIdNumber = strstudentIdNumber;
        As1Grade = strAs1Grade;
        As2Grade = strAs2Grade;
        isPresent = strIsPresent;
    }

    public void printCourseStudents() {
        System.out.println("Student Name: " + firstName + " " + lastName);
        System.out.println("Student Id: " + studentIdNumber);
        System.out.println("Assignment 1 Grade: " + As1Grade);
        System.out.println("Assignment 2 Grade: " + As2Grade);
        System.out.println("\n");
    }
    
    public String getFullName(){
        return firstName + lastName;
    }
}
